<?php
namespace Eyrots\Provider\Elasticsearch;

use \Pimple\ServiceProviderInterface;
use \Pimple\Container;
use \Elasticsearch\ClientBuilder;

class ElasticsearchProvider implements ServiceProviderInterface
{
  public function register(Container $container)
  {
    $this->instantiateElasticsearch($container);
  }
  protected function instantiateElasticsearch(Container $container)
  {
    $client = ClientBuilder::create()
      ->setHosts($container['settings']['elasticsearch.hosts'])
      ->build();
    $container['elasticsearch'] = $client;
  }
}